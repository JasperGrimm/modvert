<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 16.09.15
 * Time: 1:53
 */

include __DIR__ . "/../bootstrap.php";
$command = null;

\Qst\App::run($app, $conf, $command);

$rm = new \Qst\ResourceManager();
$rm->setDriver(new \Qst\Driver\DatabaseDriver());

$snippets = $rm->get(\Qst\IModxResource::TYPE_SNIPPET);

$snippet = $snippets[0];

$sn = new \Qst\Model\Snippet($snippet);

$s = new \Qst\PHPSerializer();
$s->serialize($sn);


$chunks = $rm->get(\Qst\IModxResource::TYPE_CHUNK);

$chunk = $chunks[0];

$ch = new \Qst\Model\Chunk($chunk);

$s = new \Qst\HTMLSerializer();
$s->serialize($ch);


$ch = new \Qst\Model\Chunk();
$ch->deserialize(\Qst\App::config('storage') . 'chunk/WebLoginSideBar.html.model');
dd($ch->toArray());

//dd($content);
//dd(eval($docblock));


