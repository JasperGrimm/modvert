<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 09/09/15
 * Time: 21:40
 */

use Symfony\Component\HttpFoundation as SHttp;

require_once __DIR__ . "/../bootstrap.php";
$command = null;
$args = [];
if (PHP_SAPI === 'cli') {
    if ($argc > 1) {
        $command = $argv[1];
        $args = array_splice($argv, 2);
    }
    if (!$args) { $args = [];}
    $args['context'] = 'cli';
    $app->bind('request', function() { return null; });
    $app->bind('response', function() { return null; });
} else {
    $args['context'] = 'web';
    $app->bind('request', function() {
        $request = SHttp\Request::createFromGlobals();
        return $request;
    });
    $app->bind('response', function() {
        $response = new SHttp\Response('Content', 200, ['content-type' => 'application/json; charset=utf-8']);
        return $response;
    });
}

\Qst\App::run($app, $conf, $command, $args);

