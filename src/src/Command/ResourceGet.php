<?php
/**
 * Created by PhpStorm.
 * User: Jasper
 * Date: 10/7/2015
 * Time: 9:19 PM
 */

namespace Qst\Command;

use Qst\App;

class ResourceGet extends Command
{

    public static function run($args=[])
    {
        $type = App::request()->query->get('type');
        $rm = new \Qst\ResourceManager();
        $rm->setDriver(new \Qst\Driver\DatabaseDriver());
        $collection = $rm->get($type);
        $r = App::response();
        $r->setContent(json_encode($collection));
        $r->send();
        die();
    }

}