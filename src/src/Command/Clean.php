<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 30/09/15
 * Time: 15:34
 */

namespace Qst\Command;


use Qst\App;

class Clean extends Command
{

    public static function run($args=null)
    {
        App::db()->table('modvert_history')->truncate();
        App::console()->writeln('<info>История успешно сброшена.</info>');
    }

}