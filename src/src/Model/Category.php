<?php
/**
 * Created by PhpStorm.
 * User: Jasper
 * Date: 10/9/2015
 * Time: 9:28 PM
 */

namespace Qst\Model;


use Qst\ResourceModel;
use Qst\Serializer\Serializer;
use Qst\Serializer\SimpleSerializer;

class Category extends ResourceModel
{

    protected $tablename = 'modx_categories';

    protected $type = 'category';

    /**
     * @var Serializer
     */
    protected $serializer;


    /**
     * @param array|null $data
     */
    public function __construct(array $data=null)
    {
        parent::__construct($data);
        $this->serializer = new SimpleSerializer();
    }

}