<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 10/09/15
 * Time: 00:56
 */

namespace Qst\Model;


use Qst\ResourceModel;
use Qst\Serializer\Serializer;
use Qst\Serializer\PHPSerializer;

class Snippet extends ResourceModel
{

    protected $tablename = 'modx_site_snippets';

    protected $type = 'snippet';

    /**
     * @var Serializer
     */
    protected $serializer;


    /**
     * @param array|null $data
     */
    public function __construct(array $data=null)
    {
        parent::__construct($data);
        $this->serializer = new PHPSerializer();
    }

    public function loadFromArray(array $data)
    {
        $this->data = $data;
        $this->data['snippet'] = preg_replace('/\\r\\n/s', "\n", $this->data['snippet']);
//        $this->data['snippet'] = preg_replace('/^(\\n*)$/sm', '', $this->data['snippet']);
    }
}