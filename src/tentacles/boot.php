<?php
/**
 * Created by PhpStorm.
 * User: jasper
 * Date: 04/10/15
 * Time: 15:46
 */

use \Symfony\Component\HttpFoundation as SHttp;
$host = $_SERVER['HTTP_HOST'];

preg_match('/(test|staging)\.questoria\.(loc|ru|com|com\.ua|kz|by|net)/', $host, $hdata);

if (count($hdata) !== 3) {
    die('Этот скрипт не может быть запущен на рабочем сайте!');
}
$subhost = $hdata[1];
$domain = $hdata[2];
if ($subhost === 'test') {
    if ($domain === 'loc') {
        $env = 'testing';
    } else {
        $env = 'staging';
    }
} elseif ($subhost === 'staging') {
    $env = 'production';
} else {
    die('Этот скрипт не может быть запущен на неизвестном сайте!');
}

defined('ENV') || define('ENV', $env);
include __DIR__ . '/../bootstrap.php';

\Qst\App::run($app, $conf);