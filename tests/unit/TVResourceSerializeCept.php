<?php 
$I = new UnitTester($scenario);
$I->wantTo('perform actions and see result');

$resourceManager = new \Qst\ResourceManager();
$remote_url = \Qst\App::config('stages.development.remote_url');
$resourceManager->setDriver(new \Qst\Driver\DatabaseDriver($remote_url));

$tv = $resourceManager->findOneByName(\Qst\IModxResource::TYPE_TEMPLVAR, 'Scenario');

$tv->serialize();

$expected_content = <<<'TV'
<?php
return [
'id' => '6',
'type' => 'dropdown',
'name' => 'Scenario',
'caption' => 'Сценарий',
'description' => '',
'editor_type' => '0',
'category' => '10',
'locked' => '0',
'elements' => '@SELECT name, scenario_id FROM qst_scenario',
'rank' => '0',
'display' => '',
'display_params' => '',
'default_text' => '',
'templates' => [
11
],
'content_values' => [
'48' => '2',
'63' => '1',
'64' => '3',
'65' => '4',
'66' => '5',
'67' => '6',
'68' => '7',
'144' => '8',
'157' => '9',
'183' => '10',
'194' => '17',
'205' => '18',
'206' => '19',
'217' => '24',
'222' => '25',
'232' => '26',
'248' => '27',
'250' => '28',
'252' => '29',
'255' => '30',
],
];
TV;


$I->assertEquals($expected_content, file_get_contents($tv->getBoundFile()));